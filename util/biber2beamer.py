#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    author:  Nils Olofsson
    date:    2020-01-17

    Parse a Latex biber file eg. 'bibliography.bib' and creates a beamer
    formatted companion bibliography file 'bibliography.tex'.
"""

from datetime import date

class Reference:
    """
        A biber reference that contains some typical entries and can be exported
        to a formatted beamer entry.
    """

    def __init__(self, biberstring=''):
        self.name      = ''
        self.pubtype   = ''
        self.entry     = ''
        self.title     = ''
        self.author    = ''
        self.booktitle = ''
        self.journal   = ''
        self.volume    = ''
        self.number    = ''
        self.pages     = ''
        self.year      = ''
        self.publisher = ''

        self.school    = '' # For theses. Could be 'organization' instead?

        if biberstring:
            self.parse(biberstring)

    def __lt__(self, other):
        return self.author < other.author

    def valid(self):
        return self.name and self.entry and self.title and self.author and self.year

    def parse(self, biberstring):
        """
        Parse biber entry with typical format:
            @PUBTYPE{NAME,
                title={TITLE},
                author={AUTHOR},
                journal={JOURNAL},
                volume={VOLUME},
                pages={PAGES},
                year={YEAR},
                publisher={PUBLISHER}
            }
        """

        values = biberstring.split('\n')
        
        p0 = values[0].find('{')
        p1 = values[0].find(',')
        
        self.pubtype = values[0][0:p0]
        self.name    = values[0][p0+1:p1]

        values = values[1:]
        for entry in values:
            entry = entry.strip()
            if len(entry) == 0:
                continue
            
            pair = entry.split('=')
            if len(pair) != 2:
                continue

            key = pair[0]
            
            p0 = pair[1].find('{')+1
            p1 = pair[1].rfind('}')
            val = pair[1][p0:p1]

            if key=='title':
                self.title = val
            elif key=='author':
                self.author = val
            elif key=='booktitle':
                self.booktitle = val
            elif key=='journal':
                self.journal = val
            elif key=='volume':
                self.volume = val
            elif key=='pages':
                self.pages = val
            elif key=='publisher':
                self.publisher = val
            elif key=='year':
                self.year = val
            elif key=='school':
                self.school = val
        # Remove digits, ie. the year, from the biber entry name
        self.entry = ''.join([i for i in self.name if not i.isdigit()])

    def export(self):
        """    
            Export to format:
                \newcommand{\ENTRY}{
                \beamertemplatearticlebibitems
                \bibitem{NAME}
                AUTHOR
                \newblock{\emph{TITLE}}
                \newblock{JOURNAL, vol. VOLUME, p. PAGES, PUBLISHER, YEAR}
        """
        
        str = ''
        str += '\\newcommand{\\' + self.entry + '}{\n'
        str += '    \\beamertemplatearticlebibitems\n'
        str += '    \\bibitem{' + self.name + '}\n'
        str += '    ' + self.author + '\n'
        str += '    \\newblock{\\emph{' + self.title + '}}\n'
        str += '    \\newblock{'
        if self.booktitle:
            str += self.booktitle + ', '
        if self.journal:
            str += self.journal + ', '
        if self.volume:
            str += self.volume + ', '
        if self.number:
            str += self.number + ', '
        #if self.pages:
        #    str += self.pages + ', '
        if self.publisher:
            str += self.publisher + ', '
        if self.pubtype=='phdthesis':
            str += 'PhD Thesis'
            if self.school:
                str += ', ' + self.school
            str += ', '
        str += self.year

        str += '}\n'
        str += '}\n'

        return str

# ==============================================================================
# Parse and export the references
# ==============================================================================

def parse(bibfile):

    content = ''
    with open(bibfile, 'r') as infile:
        for line in infile:
            if line[0] is not '#' and line.strip():
                content += line
    biberentries = content.split('@')

    references = list()
    for bibentry in biberentries:
        ref = Reference(bibentry)
        if ref.valid():
            references.append(ref)
    references.sort()

    return references

def makeReferences(references):
    str = ''
    for i, ref in enumerate(references):
        if not ref.valid():
            continue
        str += '% --------------------------------------------------------------------\n'
        str += '% Nr. {}\n'.format(i+1)
        str += '% --------------------------------------------------------------------\n'
        str += ref.export()
        str += '\n'
    return str

def makeHeader():
    str =   '% Author:   Nils Olofsson\n'
    str +=  '% Date:     {}\n'.format(date.today().strftime('%Y-%m-%d'))
    str +=  '%\n'
    str +=  '% Latex formatted references for use with complementary bibliography.bib file\n'
    str +=  '% in a beamer style presentation. The corresponding \\bibitems must be defined in\n'
    str +=  '% the .bib file. The order is set manually for the refence pages but is\n'
    str +=  '% automatic for the on-slide references using the .bib file and \\cite along\n'
    str +=  '% with \\footnote.\n'
    str +=  '% Auto-generated.\n'
    str +=  '%\n'
    str +=  '% Build order for correct beamer references is:\n'
    str +=  '%     pdflatex, biber, pdflatex\n\n'
    return str

def makeFooter(references):

    vspace = '\\vspace*{\\fill}\n'
    tab    = '    '
    str = ''
    str += '% ===============================================================================\n'
    str += '%\n'
    str += '% Create the reference pages\n'
    str += '%\n'
    str += '% ===============================================================================\n'
    str += '\\newcommand\n'
    str += '\\addBibliogrpahy{\n\n'
    str += '\\section*{}\n'
    str += '\\begin{frame}[allowframebreaks]\n'
    str += '    \\frametitle<presentation>{References}\n'
    str += '    \\begin{thebibliography}{8}\n'
    str += '    \\tiny\n\n'

    str += tab + vspace
    for i, ref in enumerate(references):
        if i!=0 and (i%4)==0:
            str += tab + vspace
            str += '\n    \\newpage\n\n'
            str += tab + vspace
        str += tab + tab + '\\' + ref.entry + '\n'
    str += tab + vspace
    
    str += '\n    \\end{thebibliography}\n'
    str += '\\end{frame}\n\n'
    str += '} % End of bibliography pages\n'
   
    return str

if(__name__=="__main__"):

    biberfile = 'bibliography.bib'
    beamfile  = biberfile.split('.')
    beamfile  = '{}.tex'.format(beamfile[0])

    print('Parsing "{}" ...'.format(biberfile))
    references = parse(biberfile)
    print('Number of entries: {}'.format(len(references)))
    str = ''
    str += makeHeader()
    str += makeReferences(references)
    str += makeFooter(references)
    with open(beamfile,'w') as outfile:
        outfile.write(str)
    print('Reference file "{}" created!'.format(beamfile))
