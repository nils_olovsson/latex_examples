#!/bin/bash
# texmake.sh
#
#

# --------------------------------------------------------------------
# display_help()
# --------------------------------------------------------------------
display_help() {
    echo 'TexMake'
    echo 'This is a script for bash compilation of latex documents with references and such.'
    echo ''
    echo 'To compile foo.tex file into foo.pdf do:'
    echo './texmake.sh -f foo.tex'
    echo ''
    echo 'Optional flags are:'
    echo '-view          Open the compiled document in a pdf viewer'
    echo '-clean         Remove latex build output files'
    echo '-use-biber     Use biber instead of bibtex for bibliography'
    echo '-ps2pdf        Use dvips and ps2pdf to compile document instead of pdflatex'
    echo '-beamer        Same as -use-biber since I use biber for my beamer presentations'
    echo '-no-log        Remove the log file log.txt after build'
    echo '-h  or --help  Display this message'
}

# ====================================================================
# Read command line arguments
# ====================================================================
filename="unknown"
pdfname="unknown"
view=false
clean=false
d_help=false
use_biber=false
use_ps2pdf=false
no_log=false

while test $# -gt 0
do
    if [ $1 == "-f" ]
    then
        shift
        filename="$1"
        pdfname="${filename:0:-4}.pdf"
    elif [ $1 == "-view" ]
    then
        view=true
    elif [ $1 == "-clean" ]
    then
        clean=true
    elif [ $1 == "-use-biber" ] || [ $1 == "-beamer" ]
    then
        use_biber=true
    elif [ $1 == "-ps2pdf" ]
    then
        use_ps2pdf=true
    elif [ $1 == "-no-log" ]
    then
        no_log=true
    elif [ $1 == "-h" ] || [ $1 == "--help" ]
    then
        d_help=true
    fi

    shift
done

# --------------------------------------------------------------------
# bib_file_exists()
# --------------------------------------------------------------------
bib_file_exists() {
    bibfile=echo ls *.bib
    if [ -f $bibfile ]
    then
        return true
    else
        return false
    fi
}

# --------------------------------------------------------------------
# compile_pdf(doc_name)
# --------------------------------------------------------------------
compile_pdf() {
    
    if [ $use_ps2pdf == true ]; then
        latex -halt-on-error "${1}.tex" > log.txt
        dvips -q "${1}.dvi"
        ps2pdf "${1}.ps"
    else
        pdflatex -halt-on-error "${1}.tex" > log.txt
    fi
}

# --------------------------------------------------------------------
# make_report()
# Build full report
# --------------------------------------------------------------------
make_pdf() {
    printf "making ${pdfname} ... "
    doc_name="${filename:0:-4}"

    # Build (two or three times in the end to get bibliography and references)
    compile_pdf $doc_name

    if [ bib_file_exists ] && [ -f "${pdfname}" ]; then
        if [ $use_biber = true ]; then
            biber "${doc_name}.bcf" > log.txt
        else
            bibtex "${doc_name}.aux" > log.txt
        fi
    fi

    if [ -f "${pdfname}" ]; then
        printf " ... "
        compile_pdf $doc_name
        printf " ... "
        compile_pdf $doc_name
        printf " done!\n"
    else
        printf "error!\n"
        echo "An error occured!"
    fi
    echo ""

    # Output warnings and errors of final build
    while read line; do
        #echo $line || grepp 'Latex Warning:'
        if [[ "$line" == "LaTeX Warning"* ]]; then
            echo $line
        elif [[ $line == "!"* ]]; then
            echo $line
        fi
        #echo $line
    done <log.txt

    echo ""
    #if [ -f ${pdfname} ]; then
        # Ouput number of pages in compiled document
        #pdftk "${pdfname}" dump_data output | grep -i NumberOfPages:
    #fi

}

# --------------------------------------------------------------------
# view_pdf()
# Build full report
# --------------------------------------------------------------------
view_pdf() {
    # Open silently in default pdf-reader by releasing it from the terminal
    if [ -f "${pdfname}" ]; then
        xdg-open "${pdfname}" &>/dev/null & disown
        echo ""
    fi
}

# --------------------------------------------------------------------
# make_clean()
# Clean up build directory
# --------------------------------------------------------------------
make_clean() {
    # Note: Can use same expression but with
    # f=`find -not \( ... \ ) -type f
    # to find files that do not correspond to certain types
    
    # Orindary latex temporary files
    f1=`find \( -name "*.aux" -o -name "*.bbl" -o -name "*.blg" -o -name "*.brf" -o -name "*.lbl" \) -type f`

    # Files created when compiling a beamer presentation
    f2=`find \( -name "*.bcf" -o -name "*.snm" -o -name "*.toc" -o -name "*.out" -o -name "*.nav" -o -name "*.run.xml" \) -type f`
    
    f=$f1" "$f2
    if [ "${f}" != "" ]; then
        rm $f
    fi
}

# ==============================================================================
# Main
# ==============================================================================

main() {
    if [ $d_help == true ]; then
        display_help
        return
    fi
    echo $filename
    echo $pdfname

    if [ "${filename}" != "unknown" ]; then
        if [ ! -f "${filename}" ]; then
            echo 'error: file "${filename}" not found.'
            return
        else
            make_pdf
            if [ $no_log == true ]; then
                rm -f log.txt
            fi
        fi
    fi

    if [ $clean == true ]; then
        make_clean
    fi

    if [ $view == true ] && [ "${filename}" != "unknown" ]; then
        view_pdf
    fi
}
main
